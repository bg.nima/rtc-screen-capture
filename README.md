Installation
=======
```bash
$ npm install
```

Deployment
======
1- run development server
```bash
$ npm start
```
2- install chrome extention provided in `./extention` folder as an unpacked extention on your chrome (this is the specific extention to allow requests from localhost and deffers from the one that you install form google extention store)

3- open app in your browser, default url is http://localhost:8080